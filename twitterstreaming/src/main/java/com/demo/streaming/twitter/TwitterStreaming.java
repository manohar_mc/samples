package com.demo.streaming.twitter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.twitter.TwitterUtils;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;

import twitter4j.Status;

public class TwitterStreaming
{
	private static final Logger logger = Logger.getLogger(TwitterStreaming.class.getName());

	public static void main( final  String[] args )
	{
		System.setProperty("twitter4j.oauth.consumerKey","XXXXX");
		System.setProperty("twitter4j.oauth.consumerSecret","XXXX");
		System.setProperty("twitter4j.oauth.accessToken","XXXX");
		System.setProperty("twitter4j.oauth.accessTokenSecret","XXXX");


		//String [] filters = new String[4];
		//filters[1]="bigdata";
		//filters[2]="analytics";
		//filters[2]="datascience";
		//filters[2]="hadoop";
		//filters[2]="hadoop";
		//filters[3]="javascript";

		final SparkConf sparkConf = new SparkConf().setAppName("TwitterStreaming");
		sparkConf.set("spark.executor.extraJavaOptions","-Dlog4j.configuration=file:log4j.properties");
		sparkConf.set("spark.driver.extraJavaOptions","-Dlog4j.configuration=file:log4j.properties");


		JavaStreamingContext jssc = new JavaStreamingContext(sparkConf, Durations.seconds(10));

		JavaReceiverInputDStream<Status> tstream =  null;
		if(args.length > 0)
			tstream = TwitterUtils.createStream(jssc, args);
		else
			tstream = TwitterUtils.createStream(jssc);

		JavaDStream<List<Map<String,String>>> tweets =  tstream.map(new Function<Status,List<Map<String,String>>>(){

			public List<Map<String,String>> call(Status v1) throws Exception {

				StringBuilder builder = new StringBuilder();

				List<Map<String,String>> twtlist = new ArrayList();
				System.out.println("in to srewM Mp");
				if(v1.getText().trim().length() > 0){
					int len = v1.getHashtagEntities().length;
					List<String> tags = new ArrayList();
					for(int i=0;i<len;i++)
					{
						tags.add(v1.getHashtagEntities()[i].getText());
					}


					for(String tag:tags)
					{
						tag = tag.toLowerCase();
						Map<String,String> tweet = new HashMap<String,String>();

						tweet.put("technology",tag.toLowerCase());
						tweet.put("date", String.valueOf(System.currentTimeMillis()));
						tweet.put("text", v1.getText());

						if(v1.getPlace() != null)
						{
							builder.append("Country-->").append(v1.getPlace().getCountry()).append("\n");
						}
						if(v1.getUser() != null){
							//builder.append("Text-->").append(v1.getText()).append("\n");
							tweet.put("name", v1.getUser().getName());
						}
						tweet.put("id",UUID.randomUUID().toString());
						twtlist.add(tweet);

						System.out.println(tweet);
					}

				}

				return twtlist;
			}
		});



		tweets.foreach(new Function<JavaRDD<List<Map<String,String>>>, Void>(){

			public Void call(JavaRDD<List<Map<String, String>>> v1)
					throws Exception {

				JavaSparkContext jsc = new JavaSparkContext(v1.context());

				long startTime=System.currentTimeMillis();
				List<List<Map<String,String>>> collections = v1.collect();
				System.out.println("time to collect " + (System.currentTimeMillis() - startTime));

				if(!collections.isEmpty()){
					startTime=System.currentTimeMillis();
					List<Map<String,String>> finaltweets = new ArrayList();
					for(int i=0;i<collections.size();i++)
					{
						List<Map<String,String>> intermediates = collections.get(i);
						for(Map<String,String> intermediate:intermediates)
						{
							finaltweets.add(intermediate);
						}
					}
					System.out.println("time to process " + (System.currentTimeMillis() - startTime));

					startTime = System.currentTimeMillis();
					JavaRDD<Map<String,String>> rdd = jsc.parallelize(finaltweets);
					Map<String, String> idmap = new HashMap();
					//idmap.put("es.mapping.id", "id");
					idmap.put("es.nodes", "164.99.175.174:9200");
					idmap.put("es.resource.write", "twitter/tweets");
					JavaEsSpark.saveToEs(rdd,idmap);
					System.out.println("time to store to es " + (System.currentTimeMillis() - startTime));

				}
				return null;
			}
		});

		/*tweets.foreach(new Function<JavaRDD<Map<String,String>>,Void>(){

			public Void call(JavaRDD<Map<String, String>> tweet)
					throws Exception {

				Map<String, String> idmap = new HashMap();
				//idmap.put("es.mapping.id", "id");
				idmap.put("es.nodes", "164.99.175.174:9200");
				idmap.put("es.resource.write", "twitter/tweets");
				JavaEsSpark.saveToEs(tweet,idmap);
				return null;
			}
		});*/

		//tweets.print();

		jssc.start();
		jssc.awaitTermination();

	}
}
