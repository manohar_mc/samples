package test.stackoverflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.elasticsearch.spark.rdd.api.java.JavaEsSpark;
import org.joda.time.DateTime;

import com.google.code.stackexchange.client.StackExchangeApiClient;
import com.google.code.stackexchange.client.impl.StackExchangeApiJsonClient;
import com.google.code.stackexchange.common.PagedList;
import com.google.code.stackexchange.schema.Paging;
import com.google.code.stackexchange.schema.Question;
import com.google.code.stackexchange.schema.StackExchangeSite;
import com.google.code.stackexchange.schema.TimePeriod;


public class StackOverflow
{
    public static void main( String[] args ) throws Exception
    {

    	final SparkConf conf = new SparkConf().setAppName("StackOverflowApp");
    	JavaSparkContext context = new JavaSparkContext(conf);


    	String accessToken = "accessToken";
    	String applicationKey = "XXXX";
    	StackExchangeApiClient client = new StackExchangeApiJsonClient(applicationKey, StackExchangeSite.STACK_OVERFLOW);
    	PagedList<Question> questions = null;


    	DateTime totime = new DateTime();
    	DateTime fromTime = new DateTime().minusYears(5);

    	TimePeriod  period= new TimePeriod(fromTime.toDate(), totime.toDate());
    	int pageno=1;
    	Paging page = new Paging(1,100);
    	do{

    		List<String> tags = new ArrayList();
   // 		tags.add("bigdata");
    		tags.add("hadoop");
    		questions = client.advanceSearchQuestions(null,
    				null, null, page, null,
    				tags, null, null,
    				period, null, null,
    				null, null, null, 0,
    				null, null,  null, 0);


    		List<Map<String,String>> squestions = new ArrayList();
    		for(Question question:questions)
    		{
    			String stitle = question.getTitle();
    			List<String> stags = question.getTags();
    			for(String stag:stags)
    			{
    				Map<String,String> squestion = new HashMap<String,String>();
    				squestion.put("technology",stag.toLowerCase());
    				squestion.put("date", String.valueOf(System.currentTimeMillis()));
    				squestion.put("text", stitle);
    				squestion.put("name",question.getOwner().getDisplayName());
    				squestions.add(squestion);
    			}
    		}

    		JavaRDD<Map<String,String>> rdd = context.parallelize(squestions);

    		Map<String, String> idmap = new HashMap();
			idmap.put("es.nodes", "XXXXX:9200");
			idmap.put("es.resource.write", "stackoverflow/questions");
			JavaEsSpark.saveToEs(rdd,idmap);
    		Thread.sleep(1000);
    		if(questions.isEmpty() && questions.hasMore() == false)
    			break;
    		else
    			page = new Paging(++pageno,100);
    	}while(true);
    }
}
