package test.facebook;

import java.util.Date;

import facebook4j.Comment;
import facebook4j.Facebook;
import facebook4j.FacebookFactory;
import facebook4j.Page;
import facebook4j.Post;
import facebook4j.PostUpdate;
import facebook4j.Reading;
import facebook4j.ResponseList;
import facebook4j.conf.ConfigurationBuilder;

/**
 * Hello world!
 *
 */
public class FacebookTest
{
	public static void printhashtags(String message)
	{
		if(message == null){
			return;
		}
		String []words = message.split(" ");
		for(int i=0;i<words.length;i++){


			if(words[i].trim().length() > 0 && words[i].charAt(0) == '#')
			{
				System.out.println(words[i]);
			}
		}
	}

    public static void main( String[] args ) throws Exception
    {
    	ConfigurationBuilder cb = new ConfigurationBuilder();
    	cb.setDebugEnabled(true)
    	  .setOAuthAppId("XXXX")
    	  .setOAuthAppSecret("YYYYY")
    	  //2months
    	  .setOAuthAccessToken("XXXXXXXXXXXXXXXXXXXXXXX");
    	  //.setOAuthPermissions("email,publish_stream,user_posts");
    	FacebookFactory ff = new FacebookFactory(cb.build());
    	Facebook facebook = ff.getInstance();
    	//facebook.getFriends();
   // 	facebook.postStatusMessage("hello world");


    	PostUpdate update = new PostUpdate("comment it1");
    	facebook.postFeed("XXXXX", update);

    	for(Post post: facebook.getFeed("YYYY"))
    	{
    		for(Comment comment:facebook.getPostComments(post.getId()))
    		{
    			System.out.print(comment.getMessage());
    			if(comment.getFrom() != null)
    				System.out.println("--" + comment.getFrom().getName() + "--" + comment.getFrom().getId());
    		}
    	}

    	String token = facebook.getOAuthAccessToken().getToken();




    	long fromtime = new Date().getTime() - 24 * 60 * 60 * 1000;
    	long totime = new Date().getTime();

    	Reading reading = new Reading().since(new Date(fromtime)).until(new Date(totime));
    	while(true){


    		ResponseList<Page> results = facebook.searchPages("nosql", reading);
    		for(Page page:results)
    		{
    	//		facebook.postStatusMessage(page.getId(),"posting...");
    			System.out.println("****Reading page " + page.getName());

    			for(Post post: facebook.getPosts(page.getId()))
    			{
    				printhashtags(post.getMessage());
    				System.out.println(post.getUpdatedTime());
    				for(Comment comment:post.getComments())
    				{
    					printhashtags(comment.getMessage());
    				}

    			}

    			for(Post post:facebook.getFeed(page.getId()))
    			{
    				printhashtags(post.getMessage());
    				for(Comment comment:post.getComments())
    				{
    					printhashtags(comment.getMessage());
    				}
    			}
    		}
    	}
   }
}
